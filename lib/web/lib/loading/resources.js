/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Load API resources for the internal Hapi server
 * @module vlogger-web/lib/loading/resources
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires vlogger-stdlib/class
 * @requires vlogger-core/lib/loading/loader
 */
var path    = require('path')
  , Loader  = require('gaz/fs/loader')
  , Class   = require('gaz/class')
  , values  = require("gaz/object").values
  , flatten = require('gaz/array').flatten
  , Resources
  , loader
  ;

/**
 * @alias module:vlogger-web/lib/loading/resources
 * @constructor
 * @extends module:vlogger-core/lib/loading/loader
 */
Resources = new Class(/* @lends module:vlogger-web/lib/loading/resources.prototype */{
	inherits: Loader
	,options:{
		searchpath:'resources'
		,filepattern:/\.js$/
	}
	,toName: function( app, path ){
		return app;
	}
	,remap: function remap( loaded ){
		console.log( loaded );
		var resources = this.parent('remap', loaded );
		
		return resources && {
			name: path.basename( loaded.path ).replace(this.options.filepattern,''),
			resource: resources
		}; 
	}
	, flat: function flat(){
		var items = values( this )
		  , out = []
		  , next;
		  ;

		for(var x=0,len=items.length; x<len; x++){
			next = items[x];
			if( Array.isArray( next ) ){
				out.push.apply( out, next );
			}else{
				out = out.concat( next );
			}
		}
		return flatten( out )
	}
})

loader = new Resources()
/**
 * Returns An object containe all of the fixture objects listed by app name
 * @param {...String} packages Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 * @example
var resources = require('vlogger-web/lib/loading/resources')
resources.load( );
resources.load( 'vlogger' );
resources.load( 'web', 'log', 'core' )
resources.load( ).flat()
 */
exports.find = function find( ){
	return loader.find.apply(loader, arguments );
};


/**
 * Returns An object containe all of the fixture objects listed by app name
 * @param {...String} packages Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 * @example
var resources = require('vlogger-web/lib/loading/resources')
resources.load( );
resources.load( 'vlogger' );
resources.load( 'web', 'log', 'core' )
resources.load( ).flat( )
 */
exports.load = function load( ){
	return loader.load.apply( loader, arguments );
};

exports.Loader = Resources;
