var conf = require('keef');
var log  = require('bitters');
var hapi = require('hapi');
var Api = require('tastypie').Api;
var isFunction = require("gaz/lang").isFunction;
var resources = require("web/lib/loading/resources");
var port = conf.get('PORT');
var server;
var os = require('os')
var fs = require('fs')
var path = require('path');
var v1,api;

server = new hapi.Server({
	connections: {
	    routes: {
	        files: {
	            relativeTo: path.join( os.tmpDir(), 'uploads' )
	        }
	    }
	}
});
api = new Api('api/v1');

server.connection({
	host:'0.0.0.0',
	port:port,
	labels:['main']
});

resources
	.load()
	.flat()
	.forEach( function( loaded ){
		isFunction( loaded.resource ) && api.use( loaded.name, new loaded.resource )
	});	


server.route({
	method:'GET'
	,path:'/uploads/{filename}'
	,config:{
		handler: function( req, reply ){
			reply.file( req.params.filename );
		}
	}
})
if( require.main === module){
	server.register([api], function(){
		server.start(function(){
			log.info('server running at http://0.0.0.0:%s', port);
		})
	})
}
