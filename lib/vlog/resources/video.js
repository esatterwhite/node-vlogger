var fs          = require('fs');
var os          = require('os');
var tastypie    = require("tastypie");
var Resource    = require('tastypie/lib/resource/rethink');
var Video       = require('../models/video')
var interpolate = require('gaz/string').interpolate
var urljoin     = require('tastypie/node_modules/urljoin')
var fields      = require('tastypie/lib/fields');
var path        = require('path');
var http        = tastypie.http;
var Class       = tastypie.Class;
var VideoResource;


var FileField = exports.FileField = new Class({
	inherits: fields.ApiField
	, options:{
		stream: false,
		root: os.tmpDir(),
		dir:'uploads'

	}
	,constructor: function( options ){
		this.parent('constructor', options );
		try{
			fs.mkdirSync( path.join( this.options.root, this.options.dir ) );
		}catch( e ){
			// pass. don't care;
		}
	}

	, convert: function( value ){

		if( fs.existsSync( path.dirname( value ) ) ){
			return urljoin( '/', this.options.dir, path.basename( value ) ) 
		}

		throw new Error('dir does not exist');
	}

	, type: function( ){
		return 'file';
	}

	, hydrate: function( bundle ){
		var value = this.parent('hydrate', bundle )
		var fpath = path.join( this.options.root, this.options.dir,  value.hapi.filename )
		var out = fs.createWriteStream( fpath );
		value.pipe( out );
		return fpath;
	
	}
});

VideoResource = Resource.extend({
	options:{
		queryset: Video.filter({active:true})
	}
	,fields:{
		created:{ type:'date', readonly: true }
		,name: { type:'char', required: true }
		,tag_line:{ type:'char', nullable: true }
		,description:{ type:'char', required: true }
		,first_name:{type:'char', attribute:'author.first', required: true, help:"the first name of the author" }
		,last_name: {type:'char', attribute:'author.last', required:true, help:"the last name of the author"}
		,file_size:{ type:'int', readonly:true}
		,tags:{ type:'array', default:[] }
		,video:new FileField( )
		,file_type:{ type:'char', readonly: true}
	}
	, base_urls: function base_urls( ){
		return [{
			path: interpolate( decodeURIComponent( urljoin( '/', '{{apiname}}', '{{name}}', 'schema' ) ), this.options ).replace( /\/\//g, "/" )
		  , handler: this.get_schema.bind( this )
		  , name: 'schema'
		}
		, {
			path: interpolate( decodeURIComponent( urljoin( '/', '{{apiname}}', '{{name}}', '{pk}' ) ), this.options ).replace( /\/\//g, "/" )
		  , handler: this.dispatch_detail.bind( this )
		  , name: 'detail'
		}
		, {
			path: interpolate( decodeURIComponent( urljoin( '/', '{{apiname}}', '{{name}}' ) ), this.options ).replace( /\/\//g, "/" )
		  , handler: this.dispatch_list.bind( this )
		  , name: 'list'
		  , config:{
		  	payload:{
				output:'stream',
				maxBytes: 3 * Math.pow( 1024, 2 ) 
			}
		  }
		}];
	}
	, post_list: function( bundle ){
		bundle.req.payload.file_type = bundle.req.payload.video.hapi.headers['content-type']
		return this.parent('post_list', bundle );
	}
});
module.exports = VideoResource;
