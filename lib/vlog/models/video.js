
var rethink = require("thinky")({db:'vlogger'});
var type = rethink.type;
var Video;


module.exports = Video = rethink.createModel('vlogger_video',{
	name: type.string().required()
	,video:type.string()
	,created:type.date().default(function(){ return new Date() })
	,views: type.number().min(0).default(0)
	,author:{
		first: type.string()
		,last:type.string()
	}
	,file_size: type.number().min(0).default(0)
	,file_type: type.string()
	,active: type.boolean().default( true )
    ,screenshot:type.string()
	,description: type.string()
	,tagline: type.string()
	,tags:[ type.string() ]

})
